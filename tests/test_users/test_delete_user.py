import random

from hamcrest import assert_that, equal_to

from libs.api import API
from settings import ERRORS_RESPONSE_CODE


api = API()


def test_delete_user(user):
    """Test checks that user can be deleted through DELETE /users/id"""
    response = api.delete_users(expected_status_code=204, user_id=user['id'])
    # TODO: check that deleted user isn't available by GET /users


def test_delete_user_nonexistent_id():
    """Test checks that error occurs on try to delete nonexistent user."""
    response = api.delete_users(expected_status_code=404, user_id=random.randint(999999999999999, 99999999999999999))
    assert_that(response['_meta']['message'], equal_to(ERRORS_RESPONSE_CODE[404]))


def test_delete_user_no_authorization(user):
    """Test checks that unauthorized user doesn't have access to DELETE /users/id data."""
    response = api.delete_users(authorization=False, expected_status_code=401, user_id=user['id'])
    assert_that(response['result']['name'], equal_to(ERRORS_RESPONSE_CODE[401]))
