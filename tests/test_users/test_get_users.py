import json
from pkg_resources import resource_string

import jsonschema
from hamcrest import assert_that, has_entries, equal_to

from libs.api import API
from libs.common import allure_attach_json
from settings import ERRORS_RESPONSE_CODE


api = API()


def test_get_users():
    """Test checks that response of GET /users corresponds to expected json schema."""
    response = api.get_users(expected_status_code=200)
    expected_json_schema = json.loads(resource_string(__name__, '/test_data/schema.json'))
    allure_attach_json(expected_json_schema, 'Expected JSON schema')
    jsonschema.validate(response, expected_json_schema)


def test_get_users_filter_by_name():
    pass


def test_get_users_id(user):
    """Test checks that user's data is available through GET /users/{user_id}."""
    response = api.get_users(user_id=user['id'], expected_status_code=200)
    assert_that(response['result'], has_entries(user))


def test_get_user_nonexistent_id():
    pass


def test_get_users_no_authorization():
    """Test checks that unauthorized user doesn't have access to GET /users data."""
    response = api.get_users(authorization=False, expected_status_code=401)
    assert_that(response['result']['name'], equal_to(ERRORS_RESPONSE_CODE[401]))
