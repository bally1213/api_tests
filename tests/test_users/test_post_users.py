from hamcrest import assert_that, equal_to, has_entry, has_entries, has_item

from libs.api import API
from tests.test_users.test_data.data_to_post import generate_uer_all_fields, generate_user_required_fields
from settings import ERRORS_RESPONSE_CODE


api = API()


def test_post_new_user_only_required_fields():
    """Test checks that user with only required fields can be created."""
    data = generate_user_required_fields()
    response = api.post_users(data=data, expected_status_code=200)
    assert_that(response['result'], has_entries(data))


def test_post_new_user_all_fields():
    """Test checks that user with all fields can be created."""
    data = generate_uer_all_fields()
    response = api.post_users(data=data, expected_status_code=200)
    assert_that(response['result'], has_entries(data))


def test_post_new_user_empty_data():
    """Test check that it's not permitted to post new user with empty data"""
    data = {}
    response = api.post_users(data=data, expected_status_code=422)
    assert_that(response['_meta'], has_entry('message', ERRORS_RESPONSE_CODE[422]))
    assert_that(response['result'], has_item(has_entry('field', 'email')))
    assert_that(response['result'], has_item(has_entry('field', 'first_name')))
    assert_that(response['result'], has_item(has_entry('field', 'last_name')))
    assert_that(response['result'], has_item(has_entry('field', 'gender')))


def test_post_user_used_email(user):
    """Test check that it's not permitted to post user with used email"""
    data = generate_uer_all_fields()
    data['email'] = user['email']
    response = api.post_users(data=data, expected_status_code=422)
    assert_that(response['_meta'], has_entry('message', ERRORS_RESPONSE_CODE[422]))
    assert_that(response['result'], has_item(has_entry('field', 'email')))


def test_post_user_used_invalid_email():
    """Test check that it's not permitted to post user with used email"""
    data = generate_uer_all_fields()
    data['email'] = 'aaaa'
    response = api.post_users(data=data, expected_status_code=422)
    assert_that(response['_meta'], has_entry('message', ERRORS_RESPONSE_CODE[422]))
    assert_that(response['result'], has_item(has_entry('field', 'email')))


def test_post_user_no_authorization():
    """Test checks that unauthorized user doesn't have access to POST /users."""
    data = generate_user_required_fields()
    response = api.post_users(data=data, authorization=False, expected_status_code=401)
    assert_that(response['result']['name'], equal_to(ERRORS_RESPONSE_CODE[401]))
