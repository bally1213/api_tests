import random

from libs.common import generate_string


def generate_user_required_fields():
        return {
        'email': '{0}@gmail.com'.format(generate_string(10)),
        'first_name': generate_string(7, letter_only=True).title(),
        'last_name': generate_string(7, letter_only=True).title(),
        'gender': random.choice(['male', 'female']),
                }


def generate_uer_all_fields():
        return {
        'email': '{0}@gmail.com'.format(generate_string(10)),
        'first_name': generate_string(7, letter_only=True).title(),
        'last_name': generate_string(7, letter_only=True).title(),
        'gender': random.choice(['male', 'female']),
        'dob': '1962-03-13',
        'phone': '13122867084',
        'website': 'https://testtest.com',
        'address': '419 54th str Newtown, NE 06271-2363',
        'status': random.choice(['active', 'inactive'])
                }
