import allure
from pytest import fixture

from libs.api import API
from tests.test_users.test_data.data_to_post import generate_uer_all_fields

api = API()


@allure.step('Create user')
@fixture
def user():
    """Fixture creates new user and returns user's data."""
    response = api.post_users(data=generate_uer_all_fields(), expected_status_code=200)
    yield response['result']

    try:
        api.delete_users(user_id=response['result']['id'])
    except AssertionError:
        print('User was not deleted')

