import json
import random
import string

import allure


def generate_string(length, letter_only=False):
    if letter_only is True:
        s = string.ascii_letters
    else:
        s = string.ascii_letters + string.digits + '.' + '_'
    return ''.join(random.sample(s, length))


def allure_attach_json(json_to_attach, message):
    """Publish JSON as attach to allure step"""
    pretty_json = json.dumps(json_to_attach, ensure_ascii=False, indent=4, sort_keys=True)
    allure.attach(name=message, body=pretty_json, attachment_type=allure.attachment_type.JSON)
