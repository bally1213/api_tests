import allure
import requests

from libs.common import allure_attach_json
from settings import API_HOST, ACCESS_TOKEN


class UnexpectedStatusCode(requests.RequestException):
    pass


class BaseApiSteps(object):

    def __init__(self):
        self.base_url = API_HOST

    @allure.step('Preform request {method} {url}')
    def call(self, method, url, data=None, authorization=True, expected_status_code=None):
        url = self.base_url + url
        headers = {'Authorization': 'Bearer ' + ACCESS_TOKEN} if authorization is True else {}
        response = requests.request(method, url, headers=headers, data=data)
        allure_attach_json(response.json(), 'result of {0} {1}'.format(method, url))

        if expected_status_code:
            if response.status_code != expected_status_code:
                err_msg = 'Expected status code {0}, but was {1}'.format(expected_status_code, response.status_code)
                raise UnexpectedStatusCode(err_msg, response=response)

        return response.json()


class API(BaseApiSteps):

    def get_users(self, expected_status_code=None, authorization=True, user_id=None):
        url = '/users'
        if user_id:
            url += '/{0}'.format(user_id)
        return self.call('GET', url, expected_status_code=expected_status_code, authorization=authorization)

    def post_users(self, data, expected_status_code=None, authorization=True):
        url = '/users'
        return self.call('POST', url, data=data, expected_status_code=expected_status_code, authorization=authorization)

    def delete_users(self, expected_status_code=None, authorization=True, user_id=None):
        url = '/users/{0}'.format(user_id)
        return self.call('DELETE', url, expected_status_code=expected_status_code, authorization=authorization)
