API_HOST = 'https://gorest.co.in/public-api'

# Set your access token here
ACCESS_TOKEN = ''

ERRORS_RESPONSE_CODE = {
    401: 'Unauthorized',
    402: '',
    422: 'Data validation failed. Please check the response body for detailed error messages.',
    404: 'The requested resource does not exist.'
}
